﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pistola : MonoBehaviour
{

    public GameObject Bala;
    public float balarango = 0.5f;
    float siguientebala = 0.0f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        /*Creamos la condicional con la tecla que queremos que el jugador pulse para disparar*/

        if (Input.GetKeyDown(KeyCode.D) && Time.time > siguientebala)
        {
            siguientebala = Time.time + balarango;
            fire();
        }


    }
    void fire()
    {
        Instantiate(Bala, transform.position, Quaternion.identity);
    }
}
