﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class peces : MonoBehaviour
{
    [SerializeField] List<Transform> wayPoints;
    float velocidad = 6;
    float distancicambio = 0.2f;
    byte siguienteposicion = 0;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(
           transform.position,
           wayPoints[siguienteposicion].transform.position,
           velocidad * Time.deltaTime);

        if (Vector3.Distance(transform.position,
           wayPoints[siguienteposicion].transform.position) < distancicambio)
        {
            siguienteposicion++;
            if (siguienteposicion >= wayPoints.Count)
            {
                siguienteposicion = 0;
            }

        }



    }
}
