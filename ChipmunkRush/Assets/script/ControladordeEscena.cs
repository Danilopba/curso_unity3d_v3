﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControladordeEscena : MonoBehaviour
{

    public GameObject Jugador;
    public Camera CamaraJugador;
    public GameObject[] BloqueFrab;
    public float PunteroDeJuego;
    public float LugarSeguroDeGeneración = 12;
    public Text textoDeJuego;
    public bool Perder ;


    // Start is called before the first frame update
    void Start()
    {
        PunteroDeJuego = -7;
        Perder = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Jugador!=null)
        {

        
        CamaraJugador.transform.position = new Vector3(
            Jugador.transform.position.x,
            CamaraJugador.transform.position.y,
            CamaraJugador.transform.position.z);
            textoDeJuego.text = "Score:" + Mathf.Floor(Jugador.transform.position.x);
        }
        else
        {
            if(!Perder)
            {
                Perder = true;
                textoDeJuego.text += " \n You Lose. \"R\" to return";
            }
            if(Perder)
            {
                if (Input.GetKeyDown("r"))
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }
            }
        }
        while(Jugador!= null && PunteroDeJuego<Jugador.transform.position.x+LugarSeguroDeGeneración)
        {

            /*Creamos esto para que escoja de forma random entre las tres opciones que le otorgamos al random*/
            int indiceBloque = Random.Range(0, BloqueFrab.Length- 1);
            if(PunteroDeJuego<0)
            {
                indiceBloque = 17;
            }
            GameObject ObjetoBloque = Instantiate(BloqueFrab[indiceBloque]);
            ObjetoBloque.transform.SetParent(this.transform);
            Bloque bloque = ObjetoBloque.GetComponent<Bloque>();
            ObjetoBloque.transform.position = new Vector2(PunteroDeJuego + bloque.tamaño / 2, 0);
            PunteroDeJuego += bloque.tamaño;
        }
    }
}
