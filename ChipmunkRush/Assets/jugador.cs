﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jugador : MonoBehaviour
    
{
    public AudioClip Sound;
    public int FuerzaDeSalto;
    public int VelocidadDeMov;
    bool Enelsuelo = false;
    private GameObject BulletPre;
    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        /*En este partados se debe indicar cual sera la direccion hacia la que se mueva el muñeco y la fuerza con la que saltara,
         la cual tambien debemos indicar la direccion de salto. Es decir que mediante un sencillo codigo, vamos a obligar a nuesto 
        jugador a saltar.*/


        if(Input.GetKeyDown("space")&& Enelsuelo)
        {
            Enelsuelo = false;
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, FuerzaDeSalto));

            /*Esto de aqui sirve par allamar al audio en el momento que uno desee*/

            Camera.main.GetComponent<AudioSource>().PlayOneShot(Sound);
        }

        this.GetComponent<Rigidbody2D>().velocity = new Vector2(VelocidadDeMov,
           this.GetComponent<Rigidbody2D>().velocity.y);

        if (Input.GetKey(KeyCode.W))
        {
            Shoot();
        }

       
    }
    private void Shoot()
    {
        Instantiate(BulletPre, transform.position, Quaternion.identity);
    }
    /*Este bool que hemos añadido es para indicar que el cuadrado solo puede saltar una vez por cada vez que se le de al espacio, es decir 
     que no podra saltar infinitamente sino de forma limitada*/



    private void OnTriggerEnter2D(Collider2D c1)
    {
        Enelsuelo= true;
        if(c1.tag=="obstaculo")
        {
            GameObject.Destroy(this.gameObject);
        }
    }

   
}
